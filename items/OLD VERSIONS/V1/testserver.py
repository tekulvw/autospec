'''
Created on Jan 2, 2014

@author: macuser
'''

import web
import sqlite3

urls = (
    '/', 'Submit'
)

app = web.application(urls, globals())
render = web.template.render('html/', globals={'sqlite3':sqlite3, 'web':web, 'str':str, 'int':int})

class Submit(object):
    def GET(self):
        web.seeother("http://google.com")

    def POST(self):
        form = web.input(numberofprocessors=None, processorname=None, processorspeed=None, totalnumberofcores=None, serialnumber=None, modelidentifier=None, modelname=None, capacity0=None, capacity1=None, capacity2=None, capacity3=None, rate0=None, rate1=None, rate2=None, rate3=None, mediumtype0=None, mediumtype1=None, mediumtype2=None, mediumtype3=None, dvd_write=None, chipsetmodel0=None, chipsetmodel1=None, chipsetmodel2=None, ram=None)
        print form.serialnumber
        web.ctx.c.execute("INSERT INTO inventory (id, serial_number, processor_name, model_identifier, dvd_write, model_name, number_of_processors, processor_speed, processor_cores, ram, first_gpu, second_gpu, hddcapacity0, hddrate0, hddtype0) VALUES (NULL, :sn, :pn, :mi, :dw, :mn, :nop, :ps, :pc, :r, :gpu0, :gpu1, :hc0, :hr0, :ht0)",
                          {'sn':str(form.serialnumber), 'pn':str(form.processorname), 'mi':str(form.modelidentifier), 'dw':str(form.dvd_write), 'mn':str(form.modelname), 'nop':str(form.numberofprocessors), 'ps':str(form.processorspeed), 'pc':str(form.totalnumberofcores), 'r':str(form.ram), 'gpu0':str(form.chipsetmodel0), 'gpu1':str(form.chipsetmodel1), 'hc0':str(form.capacity0), 'hr0':str(form.rate0), 'ht0':str(form.mediumtype0)})
        web.ctx.conn.commit()
        idnum = str(web.ctx.c.execute("SELECT id FROM inventory WHERE (serial_number = :sn)", {'sn':str(form.serialnumber)}).fetchone()[0])
        return render.success(idnum)

def my_loadhook():
    web.ctx.conn = sqlite3.connect('db/inventory.db')
    web.ctx.c = web.ctx.conn.cursor()

if __name__ == '__main__':
    app.add_processor(web.loadhook(my_loadhook))
    app.run()