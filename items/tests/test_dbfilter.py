import sqlite3
import urllib
import ast

db_location = "../db/inventory2.db"

def fix_db():
    conn = sqlite3.connect(db_location)
    c = conn.cursor()

    dump_data = c.execute("select * from inventory").fetchall()

    for item in dump_data:
        new_data = fix_data(item[2])
        c.execute("UPDATE inventory SET data = :data WHERE serialnum = :serial", {'data':new_data, 'serial':str(item[1])})
        conn.commit()

def fix_data(data):
    ret = data
    ret = ret.replace("'", '"')
    ret = ret.replace('u"', '"')    
    return ret

fix_db()
