import ast
import urllib

data = open('sample_file.txt', 'r').readline()
print data
data = ast.literal_eval(data)

def process_data():
    final_data = ()
    for k,v in data.items():
        final_data = final_data + ((k, v),)
    
    ret = urllib.urlencode(final_data)
        
    ret = ret.replace("%27", '%22')
    ret = ret.replace('u%22', '%22')
    
    return ret

final = process_data()
print final

of = open('output_file.txt', 'w')
