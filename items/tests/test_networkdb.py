import urllib2
import json

log = logging.getLogger(__name__)
handler = logging.StreamHandler()
formatter = logging.Formatter(fmt="[%(levelname)s] [%(asctime)s] [Function: %(funcName)s] %(message)s", datefmt="%H:%M:%S")
handler.setFormatter(formatter)
handlerLogFile = logging.FileHandler("./logs/output.log")
handlerLogFile.setFormatter(formatter)
log.addHandler(handler)
log.addHandler(handlerLogFile)

rootLvl = logging.INFO

log.setLevel(rootLvl)

sampleFile = open("./sample_file.txt", 'r').read()

try:
    import ../main.py
