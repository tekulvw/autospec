curUserFile = "./user/curUser"

with open(curUserFile, 'r') as f:
    curUser = f.read()

while True:
    print "Current user is %s" % curUser

    newUser = raw_input("Enter new user: ")

    curUser = newUser

    with open(curUserFile, 'w') as f:
        f.write(curUser)
        f.flush()
    
    print "User successfully changed to %s" % curUser
    print ""
