#############################################################################
## AutoSpec - Generates Mac Specs for use by SellYourMac.com
## Copyright (C) 2013-2014  Will Tekulve (tekulvw), Sven Knobloch
## Contact: tekulvw@aol.com
##
## AutoSpec is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## AutoSpec is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.
#############################################################################

import yaml_converter as conv
import os
import sys
import string
import logging
import urllib
import urllib2
import ast
import json
import datetime
import smtplib
import re
from time import sleep

try:
    import argparse
except ImportError, e:
    print "Please run ARGPARSE.command"
    sys.exit(1)

parser = argparse.ArgumentParser(description='Mac System Profiler.')
parser.add_argument('--debug', action='store_true')
parser.add_argument('--require-itemid', action='store_true', dest='needItem')
parser.add_argument('--automate', action='store_true')
parser.add_argument('--out-file', dest='of')
#TODO add arg to demand user itemid
args = parser.parse_args()

log = logging.getLogger(__name__)
handler = logging.StreamHandler()
formatter = logging.Formatter(fmt="[%(levelname)s] [%(asctime)s] [Function: %(funcName)s] %(message)s", datefmt="%H:%M:%S")
handler.setFormatter(formatter)
handlerLogFile = logging.FileHandler("./logs/output.log")
handlerLogFile.setFormatter(formatter)
log.addHandler(handler)
log.addHandler(handlerLogFile)

rootLvl = logging.INFO

if args.debug:
    rootLvl = logging.DEBUG

log.setLevel(rootLvl)

log.debug("Debugging Mode Activated.")

try:
    import yaml
except ImportError, e:
    log.exception(e)
    sys.exit(1)
    
class Errors(object):
    def __init__(self, code, info):
        #TODO Create new RUN.command on desktop
        self.desktop_loc = "/Volumes/Macintosh HD/Users/macuser/Desktop/"
        self.send_email = "autospecsym@gmail.com"
        self.admin_email = ["tekulvw@aol.com","philip@sellyourmac.com"]
        self.contact_msg = "An administrator has been contacted at these emails: "+str(self.admin_email)+". If this is incorrect please contact the correct administrator and attach this file."
        self.no_contact_msg = "An administrator has NOT BEEN CONTACTED. If you feel this is in error, please send this file to these emails: "+str(self.admin_email)+"."
        self.code = code
        self.instructions = [
                            'Please contact admin.',
                            'Please double-click on the RUN.command file on the Desktop, it will run AutoSpec again and should not result in errors. If it still doesn\'t work please attach this file in an email to the admins: '+str(self.admin_email),
                            'Please contact an admin immediately, I mean call them immediately. The web server database is not accepting connections!',
                            'Please start USER_SERVER.command on the DeployStudio Computer and enter your inventory username.',
                            'The inventory username that was found is invalid, if you find this in error please double check spelling, and then contact the Web Developer.',
                            'The serial number and/or item ID was not found and is needed. Please double click on RUN.command and enter it manually.',
                            'There are multiple entries for this item on the Database. Please double click RUN.command and enter the ItemID manually. If the problem persists you may have to enter the information in yourself via Safari *gasp*.',
                            'The server found an invalid type, which means that the ItemID is probably incorrect. Double click on RUN.command and enter it manually.',
                            'Please make sure an administrator has been contacted.',
                            'Please double click on RUN.command and try again. If problem persists please contact and administrator.',
                            'Some unknown error has occurred. An administrator has been contacted.'
                            ]
        self.info = info
        self.password = self.get_email_password()
        self.handle()
        
    def handle(self):
        code = self.code[7:]
        
        if code.lower() == "invalid json. contact admin.":
            self.desktop_notification(code, self.instructions[0], name="Invalid JSON", contact=True)
        elif code.lower() == "json required.":
            self.desktop_notification(code, self.instructions[1], name="JSON Missing", contact=False)
        elif code.lower() == "database connection. contact admin.":
            self.desktop_notification(code, self.instructions[2], name="Failed Database Connection", contact=True)
        elif code.lower() == "no user.":
            self.desktop_notification(code, self.instructions[3], name="No User Found", contact=False)
        elif code.lower() == "invalid user.":
            self.desktop_notification(code, self.instructions[4], name="Invalid User", contact=False)
        elif code.lower() == "serial or itemid required.":
            self.desktop_notification(code, self.instructions[5], name="Missing Serial/ItemID", contact=False)
        elif code.lower() == "itemid required. no item found.":
            self.desktop_notification(code, self.instructions[6], name="Requires ItemID", contact=False)
        elif code.lower() == "itemid required. multiple entries.":
            self.desktop_notification(code, self.instructions[7], name="Multiple Entries", contact=False)
        elif code.lower() == "invalid type. check itemid":
            #TODO restart and demand itemid
            self.desktop_notification(code, self.instructions[8], name="Invalid Type", contact=False, filename="RequireItemID-RUN.command")
        elif code[:11].lower() == "type query.":
            self.desktop_notification(code, self.instructions[9], name="Type Query Error", contact=True)
        elif code[:18].lower() == "description query.":
            self.desktop_notification(code, self.instructions[9], name="Description Query Error", contact=True)
        elif code[:15].lower() == "products query.":
            self.desktop_notification(code, self.instructions[9], name="Product Query Error", contact=True)
        elif code[:10].lower() == "log query.":
            self.desktop_notification(code, self.instructions[9], name="Log Query Error", contact=True)
        elif code[:37].lower() == "functional description select failed.":
            self.desktop_notification(code, self.instructions[9], name="Functional Description Select Failure", contact=True)
        elif code.lower() == "no system information submitted.":
            self.desktop_notification(code, self.instructions[10], name="No Data Submitted", contact=False)
        else:
            self.desktop_notification(code, self.instructions[10], name="Unknown Error", contact=True)
    
    def desktop_notification(self, msg, instructions, name=None, contact=False, filename="RUN.command"):
        msg = msg+"\n\n"+instructions
        os.popen("cp errors/"+filename+" /Users/macuser/Desktop/RUN.command")
        if name is not None:
            with open(self.desktop_loc+name, 'w') as f:
                f.write(msg)
                f.flush()
                if contact:
                    try:
                        self.email_notification(msg, name)
                    except Exception, e:
                        log.exception(e)
                    f.write(self.contact_msg)
                    f.flush
        else:
            with open(self.desktop_loc+"!!!ERROR!!!", 'w') as f:
                f.write(msg)
                f.write(self.no_contact_msg)
                f.flush()
    
    def email_notification(self, msg, name=None):
        date = str(datetime.datetime.today().strftime("%d %b %Y %H:%M:%S"))
        if name is not None:    
            subject = date + str(name)
        else:
            subject = date + "UNNAMED ERROR"
        message = subject+"\n\n"+msg
        self.send(message)

    def send(self, message):
        server = smtplib.SMTP( "smtp.gmail.com", 587 )
        server.starttls()
        server.login( self.send_email, self.password )
        log.debug("EMAIL MESSAGE:\n"+message)
        for mail in self.admin_email:
            server.sendmail('AutoSpecSYM@gmail.com', mail, message)
    
    def get_email_password(self):
        with open("./PRIVATE/email_password", 'r') as f:
            password = f.readline()
        return password
            
    
class Submission(object):
    def __init__(self, data):
        '''
        Submission().__init__(data)
        
        data = data recieved from Main() Object
        
        Sets API URL and fixes data recieved before sending it off to server.
        '''
        self.needID = True if args.needItem else False
        
        with open('PRIVATE/inv_url') as f:
            self.url = f.readline()
        
        self.curUserFile = "./user/curUser"
        
        y = self.harddrive_modifier(self.gpu_modifier(data))
        #NOTICE Do not use self.fix_data until it can be fixed 11/4/14
        self.submit(y)
    
    def submit(self, data):
        '''
        Submission().submit(data)
        
        Data in any form will be sent to inventory API with the GET/POST form of data=EVERYTHING_HERE
        NO DATA PROCESSING IS DONE HERE
        
        May need modification based on implementation
        '''
        newdata = {'json':data}
        
        data = self.add_db_information(newdata)
        
        data = json.loads(json.dumps(data))
        
        send_data = self.process_data(data)
        
        log.debug("Formatted JSON Data: \n"+send_data)

        log.debug("URL Encoded Data: "+str(send_data))
        req = urllib2.Request(self.url, send_data)
        for x in xrange(5):
            try:
                resp = urllib2.urlopen(req)
                returned = resp.read()
                break
            except Exception,e:
                log.debug("DB Server is most likely down or you are not connected to the internet. A connection could not be made.")
                returned = None
                log.info("Retrying...")
                sleep(0.5)
        code = returned if returned is not None else "Error: No server response."
        log.info("Server Response: "+str(code))
        invID = re.compile(r'[a-zA-z]{2}\d{6}')
        with open('/Volumes/Macintosh HD/Users/macuser/Desktop/ItemID', 'w') as f:
            f.write(code)
            f.flush()
        if invID.search(code) == None:
            #TODO This is an error, --probably should write a file to the desktop?--
            #TODO Test with known error
            #with open("/Volumes/Macintosh HD/Users/macuser/Desktop/!!ERROR!!", 'w') as f:
                #f.write(code)
                #f.flush()
            Errors(code, data)
    
    def process_data(self, data):
        final_data = ()

        for k,v in data.items():
            final_data = final_data + ((k, v),)
        
        ret = urllib.urlencode(final_data)
        
        ret = ret.replace("%27", '%22')
        ret = ret.replace('u%22', '%22')
        
        return ret
        
    def add_db_information(self, data):
        ret = data
        
        with open(self.curUserFile, 'r') as f:
            name = f.read()
        if not args.automate:
            name = self.getEmployeeName()
        ret['username'] = name
        
        if not args.automate:
            itemID = self.getItemID()
            
        if not self.needID:
            serial = conv.search(data, 'serial number (system)', [])[0]
            ret['id'] = str(serial)
        else:
            ret['id'] = str(itemID)

        #TODO Get list from Philip and add other categories
        
        return ret
        
    def getItemID(self):
        verified = False
        
        log.info("Please enter Inventory Item ID (if any). If not provided then leave blank.")
        itemID = raw_input("> ")
        
        if itemID != "":
            self.needID = True
        else:
            return ""
        
        while not verified:
            log.info("Is this the correct Inventory Item ID, '"+itemID+"'?")
            answer = raw_input("[y/n]> ")
            if answer.lower() == "y":
                verified = True
            else:
                log.info("Inventory Item ID not verified, please try again.")
                itemID = raw_input("> ")
        return itemID
        
    def getEmployeeName(self):
        verified = False
        
        log.info("Please enter your Inventory System Username. Without this you will not get credit for your work!")
        name = raw_input("> ")
        while not verified:
            log.info("Is '"+name+"' your correct Inventory System Username?")
            answer = raw_input("[y/n]> ")
            if answer.lower() == "y":
                verified = True
            else:
                log.info("Username not verified, please try again.")
                name = raw_input("> ")
        return name
    
    def harddrive_modifier(self, data):
        '''
        Submission().harddrive_modifier(data)
        
        data = data in dict form
        
        Removes "Description" layer of Serial-ATA Data Type
        '''
        ret = data
        count = 0
        for k,v in ret['Serial-ATA'].items():
            medtype = conv.search(v, 'medium type', [])[0]
            try:
                capacity_data = conv.search(v, 'capacity', [])[0]
                capacity_data = capacity_data.split(" ")
                capacity_data[0] = int(float(capacity_data[0]))
                capacity = " ".join([str(i) for i in capacity_data[:2]])
            except IndexError,e:
                log.exception(e)
                return ret
            try:
                rate = conv.search(v, 'rotational rate', [])[0]
            except IndexError,e:
                if medtype != "Solid State":
                    rate = '7200'
                else:
                    rate = "SSD"
            ret['Serial-ATA'][k]['capacity'] = capacity
            ret['Serial-ATA'][k]['rate'] = rate
            ret['Serial-ATA'][k]['mediumtype'] = medtype
            del ret['Serial-ATA'][k]['Description']
            count+=1
        return ret
        
    def gpu_modifier(self, data):
        '''
        Submission().gpu_modifier(data)
        
        data = data in JSON format
        
        Adds VRAM number to front of chipset identifier
        '''
        ret = data

        #########################################
        ##### DEPRECATED
        #########################################
        
        return ret        

        count = 0
        for k,v in ret['Graphics/Displays'].items():
            ret['Graphics/Displays'][k]['Chipset Model'] = str(v['VRAM (Total)'])+" "+str(v['Chipset Model'])
            count+=1
        return ret
    
    def fix_data(self, data):
        '''
        Submission().fix_data(data)
        
        data = data in JSON format
        
        Lower cases each key in the dict, and replaces certain keys with more readable versions
        
        !!! DEPRECATED DO NOT USE !!!
        '''
        ret = {}
        for k,v in data.items():
            key = k.replace(" ","")
            if key.lower() == "serialnumber(system)":
                ret['serialnumber'] = v
            elif key.lower() == "capacity":
                #TODO Remove decimals
                ret['capacity'] = ' '.join(v.split(' ')[:2])
            elif key.lower() == "vram(total)":
                ret['vram'] = v
            elif key.lower() == "memory":
                ret['ram'] = v
            elif key.lower() == "dvd-write":
                ret['dvd_write'] = v
            else:
                ret[key.lower()] = v
        return ret
        

class Main(object):

    def __init__(self):
        '''
        Main().__init__()
        
        Main class which reads config files, gathers data, formats it, and submits it to DB Server on command.
        '''
        self.config_file = "./config.yml"
        
        self.config = {}
        self.data = {}

        self.load_config()
        log.debug("CONFIG:\n", self.config)

        self.data_types = self.config['default']['data_types']
        self.display_order = self.config['default']['display_order']

        log.debug("Initialization successful.")

    def submit(self):
        '''
        Main().submit()
        
        Creates a Submission object with current self.data
        '''
        Submission(self.data)

    def run(self):
        '''
        Main().run()
        
        Reads System Profiler Data Types from self.data_types and gathers data. Creates YAML Object directly from STDOUT, then generates dict
            which is modified based on keep/remove key names from config.
        '''
        log.debug("self.data_types:\n"+str(self.data_types))
        ret = {}
        for k,v in self.data_types.items():
            log.debug("DataType: "+str(k))
            log.debug(conv.convert(os.popen("system_profiler "+str(k)).read(), str(k)))
            new_data = yaml.load(conv.convert(os.popen("system_profiler "+str(k)).read(), str(k)))
            log.debug(new_data)
            new_data = conv.consolidate(new_data, self.data_types[k]['keep'])
            new_data = conv.delete_key(new_data, self.data_types[k]['remove'])
            for key, val in new_data.items():
                ret = {v['name']:val}
            if k == "SPDisplaysDataType":
                ret = self.retina_check(ret, v)
            elif k == "SPDiscBurningDataType":
                ret = self.dl_check(ret, v)
            self.data = dict(self.data.items() + ret.items())
            log.debug("DATA:\n"+self.data)

    def dl_check(self, data, config_data):
        '''
        Main().dl_check(data, config_data)
        
        data = system data in JSON format
        config_data = SPDiscBurningDataType config 
        
        Adds a "Dual Layer: True" to data if DL in dvd-write
        '''
        ret = data
        try:
            search = conv.search(ret, 'dvd-write', [])[0].split(",")
        except IndexError,e:
            return ret
        log.debug("Search Response: "+str(search))
        ret[config_data['name']]['Reads Dual Layer'] = "False"
        for item in search:
            if "DL" in item:
                ret[config_data['name']]['Reads Dual Layer'] = "True"
        return ret

    def retina_check(self, data, config_data):
        '''
        Main().retina_check(data, config_data)
        
        data = system data in JSON format
        config_data = SPDisplaysDataType config
        
        Adds "Retina Manufacturer: LG/Samsung/Not Found/NA" to data
        '''
        ret = data
        search = conv.search(ret, 'retina', [])
        if 'True' in search:
            retina = os.popen('ioreg -lw0 | grep \"EDID\" | sed "/[^<]*</s///" | xxd -p -r | strings -6').read().split("\n")
            for item in retina:
                if item[:2] == "LP":
                    ret[config_data['name']]['Retina Manufacturer'] = "LG"
                    break
                elif item[:3] == "LSN":
                    ret[config_data['name']]['Retina Manufacturer'] = "Samsung"
                    break
                else:
                    ret[config_data['name']]['Retina Manufacturer'] = "NOT FOUND"
        else:
            ret[config_data['name']]['Retina Manufacturer'] = "N/A"
        return ret

    def display_data(self):
        '''
        Main().display_data()
        
        Uses yaml_converter.output() to display system data in human readable format.
        '''
        log.info("Displaying all data.")
        for item in self.display_order:
            log.debug("item: "+item)
            try:
                x = conv.output(self.data[item], 1)
            except KeyError, e:
                log.error("ERROR: SOMETHING NOT FOUND!")
                break
            log.info("\n"+item+":\n"+x+"--------------------------------------------------------")

    def load_config(self):
        '''
        Main().load_config()
        
        Class function which loads the config file located at self.config_file
        '''
        config_text = open(self.config_file).read()
        config_load = yaml.load_all(config_text)
        for item in config_load:
            self.config = dict(self.config.items() + item.items())
        log.debug("Configuration successfully loaded.")
        self.fix_config()
        log.debug("Loaded configuration modified successfully.")

    def fix_config(self):
        '''
        Main().fix_config()
        
        Searches self.config for matching model identifiers and modifies the default config using
            new specifications.
        '''
        mods = None
        identifier = conv.search(self.get_data(['SPHardwareDataType']),'model identifier', [])[0]
        log.debug("Identifier "+str(identifier))
        version = self.no_comma(self.only_nums(identifier))
        log.debug("Version "+str(version))
        for k,v in self.config.items():
            if identifier[:-len(version)] == k:
                for key,val in self.config[k].items():
                    log.debug(key)
                    start = key.split("-")[0]
                    end = key.split("-")[1]
                    if float(version) >= float(start) and float(version) <= float(end):
                        mods = self.config[k][key]
        if not mods:
            return
        log.debug("Mods:\n"+str(mods))
        if mods['remove'] is not None:
            for key,val in mods['remove'].items():
                self.config['default'] = conv.delete_key(self.config['default'],[key])
        if mods['add'] is not None:
            for key,val in mods['add'].items():
                self.config['default']['data_types'][key] = val
                self.config['default']['data_types'][key]['name'] = mods['add'][key]['name']
        self.config['default']['display_order'] = mods['display_order']
        log.debug("Modified config:\n"+str(self.config))

    def no_comma(self, text):
        '''
        Main().no_comma(text)
        
        text = any string
        
        Replaces all commas in the string with periods.
        '''
        return text.replace(",", ".")

    def only_nums(self, text):
        '''
        Main().only_nums()
        
        text = any string
        
        Removes all letters from string (based on string.ascii_letters) leaving all other characters.
        '''
        ret = []
        for letter in text:
            if letter not in string.ascii_letters:
                ret.append(letter)
        return "".join(ret)

    def get_data(self, data_type):
        '''
        Main().get_data(data_type)
        
        data_type = array of data types used by system profiler e.g. SPHardwareDataType. MUST BE EXACTLY
            CORRECT.
        '''
        ret = {}
        for item in data_type:
            ret = dict(ret.items() + yaml.load(os.popen("system_profiler "+str(item)).read()).items())
        return ret

if __name__ == '__main__':
    x = Main()
    x.run()
    x.display_data()
    x.submit()
