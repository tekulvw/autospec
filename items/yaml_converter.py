#############################################################################
## Part of AutoSpec
## Copyright (C) 2013-2014  Will Tekulve (tekulvw), Sven Knobloch
## Contact: tekulvw@aol.com
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.
#############################################################################

import yaml
import os

def delete_hierarchy(data, ret={}):
    '''
    Returns only {Key:Val,} where type(Val) != dict
    '''
    for k,v in data.items():
        if type(v) != dict:
            ret[k]=v
        else:
            delete_hierarchy(v,ret)
    return ret

def search(start_dict, search_string, ret=[]):
    '''
    Returns array with values that have keys which match the search string. Can return multiple values.
    '''
    for key, value in start_dict.items():
        if key.lower() == search_string.lower():
            ret.append(str(value))
        if type(value) is dict:
            search(value, search_string, ret)
    return ret

def get_data(data_type):
    '''
    Returns output of system_profiler. data_type must be string.
    '''
    return yaml.load(os.popen('system_profiler ' + str(data_type)).read())

def consolidate(recursive_dict,keep_list):
    '''
    Returns dict where only Key:Val pairs are preserved if Key in keep_list.
    
    KEEPS STRUCTURED HIERARCHY.
    '''
    if recursive_dict is None:
        return {}
    key_list=recursive_dict.keys()
    for key in key_list:
        if(type(recursive_dict[key]) is dict):
            consolidate(recursive_dict[key], keep_list)
            if len(recursive_dict[key]) == 0:
                del recursive_dict[key]
        elif(key.lower() not in keep_list):
            del recursive_dict[key]
    return recursive_dict

def delete_key(recursive_dict,delete):
    '''
    Deletes Key:Val pairs if Key in delete, returns all others.
    
    KEEPS STRUCTURED HIERARCHY.
    '''
    if recursive_dict is None:
        return {}
    if delete is None:
        return recursive_dict
    key_list=recursive_dict.keys()
    for key in key_list:
        if(key.lower() in delete) or (key in delete):
            del recursive_dict[key]
        elif(type(recursive_dict[key]) is dict):
            delete_key(recursive_dict[key], delete)
    return recursive_dict

def count(line):
    '''
    Counts number of preceding spaces
    '''
    return int(len(line) - len(line.lstrip()))

def remove_blank_lines(text):
    '''
    Like the name says, gets rid of blank lines in text.
    '''
    ret = []
    for line in text:
        if line.lstrip() != "":
            ret.append(line)
    return ret

def remove_whitespace(line):
    '''
    Gets rid of preceding whitespace.
    '''
    return line.lstrip()

def value(line):
    '''
    Given "Key: Value" returns Value
    '''
    data = remove_whitespace(line)
    colon = data.index(":")
    return data[colon+1:]

def validity(text, data_type):
    '''
    Given basic YAML form from convert(), makes advanced check to guarantee correct syntax
    '''
    ret = ""
    data = text.split("\n")
    data.pop()
    sata = False
    num = 0
    for i in xrange(1, len(data)):
        if (("spserialatadatatype" in data_type.lower() or "spparallelatadatatype" in data_type.lower()) and sata == False):
            sata = True
        if data[i] != "---" and data[i-1] != "---" and value(data[i]) == "" and value(data[i-1]) != "" and count(data[i-1]) < count(data[i]):
            ret = ret[:-(len(value(data[i-1]))+1)] +"\n"+ " "*(count(data[i]))+remove_whitespace(data[i])+"\n"
        elif sata == True and count(data[i]) == 2:
            ret+=data[i][:-1]+" "+str(num)+":\n"
            num+=1
        else:
            ret+=data[i]+"\n"
    return ret

def convert(text, data_type):
    '''
    Given raw system_profiler output, converts into basic YAML which we hope is correct syntax.
    '''
    ret = ""
    changes = {0:0,}
    text.replace("(", "<")
    text.replace(")", ">")
    if "spdiscburningdatatype" not in data_type.lower():
        text = text.replace(": -", ": Not Available")
    data = remove_blank_lines(text.split("\n"))
    for i in xrange(0, len(data)):
        if i < len(data)-1:
            if count(data[i+1]) not in changes.iterkeys():
                changes[count(data[i+1])] = changes[count(data[i])] + 2
        if count(data[i]) == 0:
            ret+="---\n"
        ret+=" "*changes[count(data[i])]+remove_whitespace(data[i])+"\n"
    ret = validity(ret, data_type)
    return ret

def output(text, spacing=0):
    '''
    Given a Python dict, returns a string object thats formatted for easy reading.
    '''
    ret = ""
    for key in text.iterkeys():
        if type(text[key]) == dict:
            ret += '  ' * spacing + key + ':\n'
            ret += output(text[key],spacing + 1)
        else:
            ret += '  ' * spacing + str(key) + ': ' + str(text[key]) + '\n'
    return ret
