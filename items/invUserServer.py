import web

curUserFile = "./user/curUser"

urls = (
    '/', 'User'
)

app = web.application(urls, globals())
render = web.template.render('html/')

class User(object):
    def GET(self):
        with open(curUserFile, 'r') as f:
            curUser = f.read()
        return render.curUserPage(curUser)

if __name__ == '__main__':
    app.run()
