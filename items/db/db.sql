CREATE TABLE inventory (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	serial_number TEXT,
	processor_name TEXT,
	model_identifier TEXT,
	dvd_write TEXT,
	model_name TEXT,
	number_of_processors INTEGER,
	processor_speed TEXT,
	processor_cores INTEGER,
	ram TEXT,
	first_gpu TEXT,
	second_gpu TEXT,
	hddcapacity0 TEXT,
	hddrate0 INTEGER,
	hddtype0 TEXT
);
	