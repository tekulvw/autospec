'''
Created on Jan 2, 2014

@author: macuser
'''

import web
import sqlite3
import ast
import yaml_converter as conv
import json

urls = (
    '/', 'Submit'
)

app = web.application(urls, globals())
render = web.template.render('html/', globals={'sqlite3':sqlite3, 'web':web, 'str':str, 'int':int})

class Submit(object):
    def GET(self):
        web.seeother("http://google.com")

    def POST(self):
        form = web.input(id=None, json=None, username=None)
        idnum = None
        dicpt1 = ast.literal_eval(str(form.json))
        dicpt2 = str(form.username)
        dicpt3 = str(form.id)
        dic = {'id':dicpt3, 'username':dicpt2, 'json':dicpt1}
        serialnum = conv.search(dic['json'], 'serial number (system)', [])[0]
        print serialnum
        prev_ids = web.ctx.c.execute("SELECT id FROM inventory WHERE (serialnum == :sn)", {'sn':serialnum}).fetchall()
        if len(prev_ids) > 0:
            try:
                web.ctx.c.execute("UPDATE inventory SET data=:data WHERE (serialnum == :sn)", {'data':str(dic), 'sn':serialnum})
            except Exception,e:
                return render.error(("Serial Number: "+str(serialnum)+" was not entered into the database!"))
        else:
            web.ctx.c.execute("INSERT INTO inventory (id, serialnum, data) VALUES (NULL, :sn, :data)",{'sn':serialnum,'data':str(dic)})
        web.ctx.conn.commit()
        id = web.ctx.c.execute("SELECT id FROM inventory WHERE (serialnum == :sn)", {'sn':serialnum}).fetchone()
        return render.success(id)

def my_loadhook():
    web.ctx.conn = sqlite3.connect('db/inventory2.db')
    web.ctx.c = web.ctx.conn.cursor()

if __name__ == '__main__':
    app.add_processor(web.loadhook(my_loadhook))
    app.run()
