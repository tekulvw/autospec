cd "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd)"/items
if ! $(python -c "import yaml" &> /dev/null); then
    cd PyYAML
    echo "Installing PyYAML..."
    2>/dev/null 1>/dev/null sudo python setup.py install
    cd ..
fi
if ! $(python -c "import argparse" &> /dev/null); then
    cd argparse
    echo "Installing argparse"
    2>/dev/null 1>/dev/null sudo python setup.py install
    cd ..
fi
if ! $(python -c "import web" &> /dev/null); then
    cd lpthw-web
    echo "Installing WEB..."
    2>/dev/null 1>/dev/null sudo python setup.py install
    cd ..
fi

#sudo networksetup -addpreferredwirelessnetworkatindex en0 "SellYourMac Wifi" 0 WPA2 australia
#sudo networksetup -addpreferredwirelessnetworkatindex en1 "SellYourMac Wifi" 0 WPA2 australia
#sudo networksetup -addpreferredwirelessnetworkatindex en2 "SellYourMac Wifi" 0 WPA2 australia

#sudo networksetup -setairportnetwork en0 "SellYourMac Wifi" australia
#sudo networksetup -setairportnetwork en1 "SellYourMac Wifi" australia
#sudo networksetup -setairportnetwork en2 "SellYourMac Wifi" australia

python main.py

cd PyYAML
2>/dev/null 1>/dev/null python setup.py clean
cd ../argparse
2>/dev/null 1>/dev/null python setup.py clean
